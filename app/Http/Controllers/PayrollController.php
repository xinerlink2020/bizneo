<?php

namespace App\Http\Controllers;

use App\AmolestacionesModel;
use App\EmpleadosModel;
use App\PayrollModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class PayrollController extends Controller
{
    public function getRegions() {
        $regions = PayrollModel::select('Codigo', DB::raw('RTRIM(Descrip) AS Descrip'))->where('Cotab', 15)->where('Codigo', '<>', 0)->get();

        if($regions->isEmpty()) {
            Log::error('No existen registros asociados a la búsqueda');
            return response()->json(['Error' => 'No existen registros asociados a la búsqueda'], 500);
        }
        else {
            return response()->json($regions, 200);
        }
    }

    public function getPositions() {
        $positions = PayrollModel::select('Codigo',  DB::raw('RTRIM(Descrip) AS Descrip'))->where('Cotab', 3)->where('Codigo', '<>', 0)->get();

        if($positions->isEmpty()) {
            Log::error('No existen registros asociados a la búsqueda');
            return response()->json(['Error' => 'No existen registros asociados a la búsqueda'], 500);
        }
        else {
            return response()->json($positions, 200);
        }
    }

    public function getLocations() {
        $locations = PayrollModel::select('Codigo', DB::raw('RTRIM(Descrip) AS Descrip'))->where('Cotab', 14)->where('Codigo', '<>', 0)
            ->orderBy('Valor4', 'ASC')->orderBy('Descrip', 'ASC')->get();

        if($locations->isEmpty()) {
            Log::error('No existen registros asociados a la búsqueda');
            return response()->json(['Error' => 'No existen registros asociados a la búsqueda'], 500);
        }
        else {
            return response()->json($locations, 200);
        }
    }

    public function getNationalities() {
        $nationalities = PayrollModel::select('Codigo',  DB::raw('RTRIM(Descrip) AS Descrip'))->where('Cotab', 16)->where('Codigo', '<>', 0)->get();

        if($nationalities->isEmpty()) {
            Log::error('No existen registros asociados a la búsqueda');
            return response()->json(['Error' => 'No existen registros asociados a la búsqueda'], 500);
        }
        else {
            return response()->json($nationalities, 200);
        }
    }

    public function getContracts(Request $request) {
        if(empty($request->rut)) {
            Log::error('Debe ingresar el RUT del recurso');
            return response()->json(['Error' => 'Debe ingresar el RUT del recurso'], 400);
        }

        $rut = $request->rut;

        $rut = trim($rut);
        $rut = ltrim($rut, '0');

        if(preg_match('/^[0-9]+-[0-9Kk]{1}/', $rut)) {
            $run = explode('-', $rut);

            if($run[1] == 'k') {
                $run[1] = strtoupper($run[1]);
            }

            $M = 0;
            $S = 1;

            for(; $run[0]; $run[0] = floor($run[0]/10)) {
                $S = ($S + $run[0] % 10 * (9 - $M++%6)) % 11;
            }

            $S = $S ? $S - 1 : 'K';

            if($run[1] == $S) {
                $contract = EmpleadosModel::select('Fecha_ret')->where(DB::raw('LTRIM(rut)'), $rut)->where('Estado', 'A')
                    ->orderBy('Fecha_ret', 'DESC')->get();

                if($contract->isEmpty()) {
                    Log::error('No existen registros asociados a la búsqueda');
                    return response()->json(['Error' => 'No existen registros asociados a la búsqueda'], 500);
                }
                else {
                    return response()->json($contract, 200);
                }
            }
            else {
                Log::error('RUT no válido');
                return response()->json(['Error' => 'RUT no válido'], 400);
            }
        }
        else {
            Log::error('RUT no válido');
            return response()->json(['Error' => 'RUT no válido'], 400);
        }
    }

    public function getReprimands(Request $request) {
        if(empty($request->rut)) {
            Log::error('Debe ingresar el RUT del recurso');
            return response()->json(['Error' => 'Debe ingresar el RUT del recurso'], 400);
        }

        $rut = $request->rut;

        $rut = trim($rut);
        $rut = ltrim($rut, '0');

        if(preg_match('/^[0-9]+-[0-9Kk]{1}/', $rut)) {
            $run = explode('-', $rut);

            if($run[1] == 'k') {
                $run[1] = strtoupper($run[1]);
            }

            $M = 0;
            $S = 1;

            for(; $run[0]; $run[0] = floor($run[0]/10)) {
                $S = ($S + $run[0] % 10 * (9 - $M++%6)) % 11;
            }

            $S = $S ? $S - 1 : 'K';

            if($run[1] == $S) {
                $codigo = EmpleadosModel::select('Codigo')->where(DB::raw('LTRIM(rut)'), $rut)->get();

                if($codigo->isEmpty()) {
                    Log::error('No existen registros asociados a la búsqueda');
                    return response()->json(['Error' => 'No existen registros asociados a la búsqueda'], 500);
                }
                else {
                    $amonestaciones = AmolestacionesModel::whereIn('Codigo', $codigo)->where('Destipo', 'AMONESTACION')->get();

                    if($amonestaciones->isEmpty()) {
                        Log::error('No existen registros asociados a la búsqueda');
                        return response()->json(['Error' => 'No existen registros asociados a la búsqueda'], 500);
                    }
                    else {
                        return response()->json($amonestaciones, 200);
                    }
                }
            }
            else {
                Log::error('RUT no válido');
                return response()->json(['Error' => 'RUT no válido'], 400);
            }
        }
        else {
            Log::error('RUT no válido');
            return response()->json(['Error' => 'RUT no válido'], 400);
        }
    }
}
