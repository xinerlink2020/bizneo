<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectoModel extends Model
{
    protected $connection = 'pgsql';
    protected $table = 'comercial_ficha_proyectos';
    protected $fillable = [
        'id',
        'nombre',
        'id_payroll',
        'empresa',
        'comercial_ficha_clientes_id',
        'comercial_ficha_contratos_id',
        'descripcion',
        'fecha_ini',
        'fecha_ter',
        'fecha_ter_condicion',
        'fact_iva',
        'fact_proporcion_dias',
        'fact_fecha_factura',
        'fact_finiquitos',
        'fact_separacion',
        'fact_glosa',
        'fact_cccliente',
        'cotizacion_restrict',
        'fact_enviar',
        'fact_direccion',
        'fact_email',
        'per_asistencia',
        'per_fecha_de_pago',
        'per_tipo_sueldo',
        'proy_region',
        'proy_comuna',
        'proy_direccion',
        'sucursales_id',
        'rec_forma_pago',
        'rec_cuenta',
        'rec_descrip_forma_pago',
        'comercial_lineas_negocio_id',
        'rep_legal_opcional',
        'rep_legal_nombre',
        'rep_legal_rut',
        'finanzas_cr_seccion_id',
        'fact_tipo_calculo_id',
        'rec_banco',
        'rec_credito',
        'fact_monto_fijo',
        'fact_monto_fijo_moneda'
    ];
}
