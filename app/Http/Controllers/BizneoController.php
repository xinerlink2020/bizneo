<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class BizneoController extends Controller
{
    public function getCompanies() {
        $url = 'https://ats.bizneo.com/api/v3/users/companies';
        $user = $_ENV['API_EMAIL_USER'];
        $token = $_ENV['API_TOKEN'];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json',
            'Authorization: Token token='.$token.', user_email='.$user.''));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if($err) {
            Log::error($err);
            return response()->json(['Error', $err], 500);
        }
        else {
            $data = json_decode($response, true);
            return $data["companies"][0]["id"];
        }
    }

    public function getCandidates() {
        $idCompany = $this->getCompanies();

        $url = 'https://ats.bizneo.com/api/v3/companies/'.$idCompany.'/candidates';

        $user = $_ENV['API_EMAIL_USER'];
        $token = $_ENV['API_TOKEN'];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json',
            'Authorization: Token token='.$token.', user_email='.$user.''));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if($err) {
            Log::error($err);
            return response()->json(['Error', $err], 500);
        }

    }

    public function getCandidate(Request $request) {
        $email = $request->email;

        if(preg_match('/^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i', $email)) {
            $idCompany = $this->getCompanies();

            $url = 'https://ats.bizneo.com/api/v3/companies/' . $idCompany . '/candidates?user[emails][]=' . $email;

            $user = $_ENV['API_EMAIL_USER'];
            $token = $_ENV['API_TOKEN'];

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json',
                'Authorization: Token token='.$token.', user_email='.$user.''));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

            if($err) {
                Log::error($err);
                return response()->json(['Error', $err], 500);
            }
            else {
                $data = json_decode($response, true);

                for($i = 0; $i < count($data['candidates']); $i++) {
                    $candidates[] = [
                        'Nombre' => $data['candidates'][$i]['first_name'].' '.$data['candidates'][$i]['last_name'],
                        'Correo Electrónico' => $data['candidates'][$i]['email'],
                        'Celular' => $data['candidates'][$i]['professional_metadata']['phone'],
                        'Fecha de Nacimiento' => $data['candidates'][$i]['user_metadata']['birth_date']
                    ];
                }

                return response()->json($candidates, 200);
            }
        }
        else {
            Log::error('Correo Electrónico no válido');
            return response()->json('Correo Electrónico no válido', 400);
        }
    }

    public function createCandidate(Request $request) {
        $rules = [
            'email' => [
                'required',
                'email'
            ],
            'first_name' => [
                'required',
                'regex:/^[a-zA-Z ]+$/u'
            ],
            'last_name' => [
                'required',
                'regex:/^[a-zA-Z ]+$/u'
            ],
            'birth_date' => [
                'required',
                'date',
                'regex:/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/'
            ],
            'phone' => [
                'required',
                'numeric',
                'regex:/[0-9]{9}/'
            ],
            'rut' => [
                'required',
                'regex: /^[0-9]+-[0-9Kk]{1}/'
            ]
        ];

        $messages = [
            'email.required' => 'Debe ingresar una dirección de correo electrónico',
            'email.email' => 'Formato de correo electrónico no valido',
            'first_name.required' => 'Debe ingresar el nombre del candidato',
            'first_name.regex' => 'El nombre del candidato solo debe contener letras',
            'last_name.required' => 'Debe ingresar los apellidos del candidato',
            'last_name.regex' => 'El(los) apellido(s) del candidato solo debe(n) contener letras',
            'birth_date.required' => 'Debe ingresar la fecha de nacimiento del candidato',
            'birth_date.date' => 'La fecha de nacimiento no es valida',
            'birth_date.regex' => 'La fecha de nacimiento debe ser Año-Mes-Día',
            'phone.required' => 'Debe ingresar el número de teléfono del candidato',
            'phone.numeric' => 'Solo se permite ingresar números',
            'phone.regex' => 'El número ingresado debe ser de 9 dígitos',
            'rut.required' => 'Debe ingresar el RUT del postulante',
            'rut.regex' => 'El RUT ingresado es incorrecto'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {
            $message = $validator->messages();
            $error = $message->all();

            Log::error($error);
            return response()->json(['Error: ' => $error], 500);
        }
        else {
            $rut = $request->rut;
            $rut = trim($rut);
            $rut = ltrim($rut, '0');
            $run = explode('-', $rut);

            if ($run[1] == 'k') {
                $run[1] = strtoupper($run[1]);
            }

            $M = 0;
            $S = 1;

            for (; $run[0]; $run[0] = floor($run[0] / 10)) {
                $S = ($S + $run[0] % 10 * (9 - $M++ % 6)) % 11;
            }

            $S = $S ? $S - 1 : 'K';

            if ($run[1] == $S) {
                $data = json_encode([
                    "user" => [
                        "email" => $request->email,
                        "source" => "importer_external_portal",
                        "user_metadata_attributes" => [
                            "first_name" => $request->first_name,
                            "last_name" => $request->last_name,
                            "birth_date" => $request->birth_date/*,
                            "dni" => $request->rut*/
                        ],
                        "professional_metadata_attributes" => [
                            "phone" => $request->phone
                        ]
                    ]
                ]);

                $idCompany = $this->getCompanies();

                $url = 'https://ats.bizneo.com/api/v3/companies/' . $idCompany . '/candidates';
                $user = $_ENV['API_EMAIL_USER'];
                $token = $_ENV['API_TOKEN'];

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type:application/json',
                    'Authorization: Token token=' . $token . ', user_email=' . $user . '']);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

                $response = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);

                if($err) {
                    Log::error($err);
                    return response()->json(['Error' => $err], 500);
                } else {
                    return response()->json('Candidato registrado correctamente', 200);
                }
            }
            else {
                Log::error('RUT no válido');
                return response()->json(['Error' => 'RUT no válido'], 400);
            }
        }
    }

    public function getOffers() {
        $idCompany = $this->getCompanies();

        $url = 'https://ats.bizneo.com/api/v3/companies/' . $idCompany . '/jobs';
        $user = $_ENV['API_EMAIL_USER'];
        $token = $_ENV['API_TOKEN'];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json',
            'Authorization: Token token='.$token.', user_email='.$user.''));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if($err) {
            Log::error($err);
            return response()->json(['Error', $err], 500);
        }
        else {
            $data = json_decode($response, true);

            for($i = 0; $i < count($data['jobs']); $i++) {
                $jobs[] = [
                    'ID' => $data['jobs'][$i]['id'],
                    'Nombre' => $data['jobs'][$i]['title'],
                    'Fecha de Publicación' => $data['jobs'][$i]['order_date'],
                    'Estado' => $data['jobs'][$i]['status']
                ];
            }

            return response()->json($jobs, 200);
        }
    }

    public function getPostulants(Request $request) {
        $id = $request->id;
        $idCompany = $this->getCompanies();

        $url = 'https://ats.bizneo.com/api/v3/companies/' . $idCompany . '/jobs/' . $id . '/candidates';
        $user = $_ENV['API_EMAIL_USER'];
        $token = $_ENV['API_TOKEN'];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json',
            'Authorization: Token token='.$token.', user_email='.$user.''));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if($err) {
            Log::error($err);
            return response()->json(['Error', $err], 500);
        }
        else {
            $data = json_decode($response, true);

            for($i = 0; $i < count($data['candidates']); $i++) {
                $postulants[] = [
                    'Nombre' => $data['candidates'][$i]['first_name'].' '.$data['candidates'][$i]['last_name'],
                    'Correo Electrónico' => $data['candidates'][$i]['email'],
                    'Celular' => $data['candidates'][$i]['professional_metadata']['phone'],
                    'Fecha de Nacimiento' => $data['candidates'][$i]['user_metadata']['birth_date']
                ];
            }

            return response()->json($postulants, 200);
        }
    }
}
