<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// CANDIDATOS BIZNEO
Route::post('candidates', 'BizneoController@createCandidate');
Route::get('candidates/{email}', 'BizneoController@getCandidate');

// OFERTAS BIZNEO
Route::get('offers', 'BizneoController@getOffers');

// POSTULACIONES BIZNEO
Route::get('offers/{id}/postulants', 'BizneoController@getPostulants');

// CANDIDATOS BUSEL
Route::get('resources/{rut}', 'BuselController@getResource');

// PROYECTOS
Route::get('projects', 'SigestionController@getProjects');

// SUCURSALES
Route::get('subsidiaries', 'SigestionController@getSubsidiaries');

// CARGOS
Route::get('positions', 'PayrollController@getPositions');

// REGIONES
Route::get('regions', 'PayrollController@getRegions');

// NACIONALIDADES
Route::get('nationalities', 'PayrollController@getNationalities');

// COMUNAS
Route::get('locations', 'PayrollController@getLocations');

// CONTRATOS
Route::get('contracts/{rut}', 'PayrollController@getContracts');

// AMONESTACIONES
Route::get('reprimands/{rut}', 'PayrollController@getReprimands');

// CLIENTES (COPPER)
Route::get('clients', 'CopperController@getClients');
