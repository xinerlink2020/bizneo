<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmolestacionesModel extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'RHOJAVI';
    protected $fillable = [
        'Codigo',
        'Fecha',
        'Hora',
        'Tipo',
        'Detalle',
        'Estado',
        'Respon',
        'Nhoras',
        'Destipo',
        'Ivt',
        'Jdd'
    ];
}
