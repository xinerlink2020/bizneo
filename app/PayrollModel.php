<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollModel extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'RTABLAS';
    protected $fillable = [
        'Tipo',
        'Cotab',
        'Codigo',
        'Descrip',
        'Valor',
        'Valor2',
        'ValorC',
        'Valor4',
        'CodAl',
        'ValorG',
        'Jdd'
    ];
}
