<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CopperController extends Controller
{
    public function getClients() {
        $url = 'https://api.prosperworks.com/developer_api/v1/companies/search';
        $user = $_ENV['API_COPPER_EMAIL_USER'];
        $token = $_ENV['API_COPPER_TOKEN'];

        // Variable que contiene los primeros 200 registros (restricciones API)
        $data_page_one = json_encode(array(
            'page_size' => 200,
            'sort_by' => 'name',
            'page_number' => 1,
            'custom_fields' => [
                array(
                    'custom_field_definition_id' => 339444,
                    'value' => 558136
                )
            ]
        ));

        // Envío de datos por cURL de los primeros 200 registros
        $curl_one = curl_init();
        curl_setopt($curl_one, CURLOPT_URL, $url);
        curl_setopt($curl_one, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_one, CURLOPT_HTTPHEADER, array(
            'X-PW-AccessToken: '.$token,
            'X-PW-Application: developer_api',
            'X-PW-UserEmail: '.$user,
            'Content-Type: application/json'
        ));

        curl_setopt($curl_one, CURLOPT_POSTFIELDS, $data_page_one);

        $response_one = curl_exec($curl_one);
        $error_one = curl_error($curl_one);
        curl_close($curl_one);

        if($error_one) {
            Log::error($error_one);
            return response()->json(['Error' => $error_one], 500);
        }

        // Variable que contiene los registros restantes (restricciones API)
        $data_page_two = json_encode(array(
            'page_size' => 200,
            'sort_by' => 'name',
            'page_number' => 2,
            'custom_fields' => [
                array(
                    'custom_field_definition_id' => 339444,
                    'value' => 558136
                )
            ]
        ));

        // Envío de datos por cURL de los registros restantes
        $curl_two = curl_init();
        curl_setopt($curl_two, CURLOPT_URL, $url);
        curl_setopt($curl_two, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_two, CURLOPT_HTTPHEADER, array(
            'X-PW-AccessToken: '.$token,
            'X-PW-Application: developer_api',
            'X-PW-UserEmail: '.$user,
            'Content-Type: application/json'
        ));

        curl_setopt($curl_two, CURLOPT_POSTFIELDS, $data_page_two);

        $response_two = curl_exec($curl_two);
        $error_two = curl_error($curl_two);
        curl_close($curl_two);

        if($error_two) {
            Log::error($error_two);
            return response()->json(['Error' => $error_two], 500);
        }

        $list_one = json_decode($response_one, true);
        $list_two = json_decode($response_two, true);

        $fusion = array();
        $fusion = array_merge($list_one, $list_two);

        for($i = 0; $i < count($fusion); $i++) {
            for($j = 0; $j < count($fusion[$i]['custom_fields']); $j++) {
                if($fusion[$i]['custom_fields'][$j]['custom_field_definition_id'] == 332752) {
                    $fusion_specific[] = [
                        'Nombre' => $fusion[$i]['name'],
                        'RUT' => $fusion[$i]['custom_fields'][$j]['value']
                    ];
                }
            }
        }

        return response()->json($fusion_specific, 200);
    }
}
