<?php

namespace App\Http\Controllers;

use App\ProyectoEstadoModel;
use App\ProyectoModel;
use App\SucursalModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SigestionController extends Controller
{
    public function getSubsidiaries() {
        $sucursales = SucursalModel::select('id', 'nombre')->orderBy('zona', 'ASC')->orderBy('id', 'ASC')->get();

        if($sucursales->isEmpty()) {
            Log::error('No existen registros asociados a la búsqueda');
            return response()->json(['Error' => 'No existen registros asociados a la búsqueda'], 500);
        }
        else {
            return response()->json($sucursales, 200);
        }
    }

    public function getProjects() {
        $projects = ProyectoModel::select(DB::raw("CONCAT(RTRIM(nombre), ' (', comercial_ficha_proyectos.id, ')') AS Proyecto"))
            ->join('comercial_ficha_proyectos_estados', 'comercial_ficha_proyectos_estados.comercial_ficha_proyectos_id', 'comercial_ficha_proyectos.id')
            ->where('comercial_ficha_proyectos_estados.estado', 'Activo')
            ->where('comercial_ficha_proyectos_estados.actual', 1)
            ->orderBy('comercial_ficha_proyectos.nombre', 'ASC')
            ->get();

        if($projects->isEmpty()) {
            Log::error('No existen registros asociados a la búsqueda');
            return response()->json(['Error' => 'No existen registros asociados a la búsqueda'], 500);
        }
        else {
            return response()->json($projects, 200);
        }
    }

}
