<?php

namespace App\Http\Controllers;

use App\BuselModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BuselController extends Controller
{
    public function getResource(Request $request) {
        if(empty($request->rut)) {
            Log::error('Debe ingresar el RUT del recurso');
            return response()->json(['Error' => 'Debe ingresar el RUT del recurso'], 400);
        }

        $rut = $request->rut;

        $rut = trim($rut);
        $rut = ltrim($rut, '0');

        if(preg_match('/^[0-9]+-[0-9Kk]{1}/', $rut)) {
            $run = explode('-', $rut);

            if($run[1] == 'k') {
                $run[1] = strtoupper($run[1]);
            }

            $M = 0;
            $S = 1;

            for(; $run[0]; $run[0] = floor($run[0]/10)) {
                $S = ($S + $run[0] % 10 * (9 - $M++%6)) % 11;
            }

            $S = $S ? $S - 1 : 'K';

            if($run[1] == $S) {
                $recurso = BuselModel::select('nombre', 'apellido_paterno', 'apellido_materno', 'fecha_nacimiento', 'celular')->where('rut', $rut)->get();

                if($recurso->isEmpty()) {
                    Log::error('No existen registros asociados a la búsqueda');
                    return response()->json(['Error' => 'No existen registros asociados a la búsqueda'], 500);
                }
                else {
                    return response()->json($recurso, 200);
                }
            }
            else {
                Log::error('RUT no válido');
                return response()->json(['Error' => 'RUT no válido'], 400);
            }
        }
        else {
            Log::error('RUT no válido');
            return response()->json(['Error' => 'RUT no válido'], 400);
        }
    }
}
