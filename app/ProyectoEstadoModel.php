<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectoEstadoModel extends Model
{
    protected $connection = 'pgsql';
    protected $table = 'comercial_ficha_proyectos_estado';
    protected $fillable = [
        'id',
        'estado',
        'actual',
        'user_id',
        'created',
        'comercial_ficha_proyectos_id'
    ];
}
