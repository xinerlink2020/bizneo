<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SucursalModel extends Model
{
    protected $connection = 'pgsql';
    protected $table = 'sucursales';
    protected $fillable = [
        'id',
        'nombre',
        'estado',
        'dirección',
        'zona',
        'telefono1',
        'telefono2',
        'telefono3',
        'codigo_alt'
    ];
}
